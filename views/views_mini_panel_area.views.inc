<?php
/**
 * @file
 * Provide views data and handlers for views_block_area.module
 */

/**
 * Implements hook_views_data().
 */
function views_mini_panel_area_views_data() {
  $data['views']['mini_panel'] = array(
    'title' => t('Mini panel'),
    'help' => t('Insert a mini panel inside an area.'),
    'area' => array(
      'handler' => 'views_handler_area_mini_panel',
    ),
  );
  return $data;
}

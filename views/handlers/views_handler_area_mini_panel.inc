<?php
/**
 * @file
 * Block area handlers. Insert a block inside of an area.
 */

// @codingStandardsIgnoreStart
/**
 * Class views_handler_area_mini_panel
 */
class views_handler_area_mini_panel extends views_handler_area {

  /**
   * The options saved with the plugin.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['mini_panel'] = array('default' => '');
    return $options;
  }

  /**
   * Options to allow admin to select mini panel.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $minis = panels_mini_load_all();
    $options = array();
    foreach ($minis as $mini_panel) {
      $options[$mini_panel->name] = ($mini_panel->category ? $mini_panel->category : 'Mini Panel') . ': ' . $mini_panel->admin_title;
    }
    $form['mini_panel'] = array(
      '#type' => 'select',
      '#title' => t('Mini panel'),
      '#default_value' => $this->options['mini_panel'],
      '#description' => t('The mini panel to insert into this area.'),
      '#options' => $options,
    );
  }

  /**
   * Render the area.
   */
  public function render($empty = FALSE) {
    if ((!$empty || !empty($this->options['empty'])) && !empty($this->options['mini_panel'])) {
      $panel_id = $this->options['mini_panel'];
      $panel = module_invoke('panels_mini', 'block_view', $panel_id);
      return $panel['content'];
    }
    return '';
  }
}
// @codingStandardsIgnoreEnd
